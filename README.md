# Specyfikacja

Urządzenie jest termometrem/czujnikiem wilgotności powietrza z interfejsem Bluetooth-LE i funkcją loggera T/RH.

# Projekt PCB

Schemat: [doc/TMP_RH_BTLE_M01_V1_0_SCH.pdf](doc/TMP_RH_BTLE_M01_V1_0_SCH.pdf)

Widok 3D: [doc/TMP_RH_BTLE_M01_V1_0_3D.pdf](doc/TMP_RH_BTLE_M01_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

Rysunek montażowy: [doc/TMP_RH_BTLE_M01_V1_0_ASSEMBLY_TOP.pdf](doc/TMP_RH_BTLE_M01_V1_0_ASSEMBLY_TOP.pdf)

![](resource/pcb_3d.png "PCB 3D")

![](resource/pcb.png "PCB top")

# Licencja

MIT
